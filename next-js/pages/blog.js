import Head from 'next/head'
import Link from 'next/link'
import axios from 'axios'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActionArea from '@material-ui/core/CardActionArea'

const Blog = ({ posts }) => {
    return (
        <Container>
            <Head>
                <title>Blog</title>
            </Head>
            {posts.map(({ id, title }) => {
                return (
                    <Card className="post" key={id}>
                        <Link href={`/post/${id}`}>
                            <CardActionArea>
                                <CardContent>
                                    <Typography variant="h6">
                                        {title}
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Link>
                    </Card>
                )
            })}
        </Container>
    )
}

export async function getServerSideProps(context) {
    const props = {
        posts: [],
    }

    try {
        const response = await axios.get(
            'https://jsonplaceholder.typicode.com/posts',
            {
                headers: {
                    Accept: 'application/json',
                },
            }
        )
        props.posts = response.data
    } catch (e) {}

    return { props }
}

export default Blog

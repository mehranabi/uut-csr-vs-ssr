import Head from 'next/head'
import Router from 'next/router'
import axios from 'axios'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Button from '@material-ui/core/Button'

const Post = ({ post }) => {
    if (!post) {
        return (
            <Typography variant="h1" color="error" align="center">
                Not found!
            </Typography>
        )
    }

    const onBackClick = () => Router.back()

    return (
        <Container>
            <Head>
                <title>{post.title}</title>
            </Head>
            <Button variant="outlined" onClick={onBackClick}>
                Back
            </Button>
            <Typography variant="body1">{post.body}</Typography>
        </Container>
    )
}

export async function getServerSideProps(context) {
    const props = {
        post: null,
    }

    const { id } = context.params
    try {
        const response = await axios.get(
            `https://jsonplaceholder.typicode.com/posts/${id}`
        )
        props.post = response.data
    } catch (e) {}

    return { props }
}

export default Post

import Head from 'next/head'
import Link from 'next/link'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

const Home = () => {
    return (
        <Container>
            <Head>
                <title>Home Page</title>
            </Head>
            <Typography variant="h4">Welcome!</Typography>
            <Link href="/blog">
                <Button variant="outlined">Blog</Button>
            </Link>
        </Container>
    )
}

export default Home

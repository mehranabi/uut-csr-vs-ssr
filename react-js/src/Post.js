import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import axios from 'axios'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'

const Post = () => {
    const { id } = useParams()
    const history = useHistory()

    const [loading, setLoading] = useState(true)
    const [post, setPost] = useState(null)

    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = async () => {
        setLoading(true)

        try {
            const response = await axios.get(
                `https://jsonplaceholder.typicode.com/posts/${id}`,
                {
                    headers: {
                        Accept: 'application/json',
                    },
                }
            )
            setPost(response.data)
        } catch (e) {}

        setLoading(false)
    }

    const onBackClick = () => history.goBack()

    return (
        <Container>
            <Button variant="outlined" onClick={onBackClick}>
                Back
            </Button>
            {loading ? (
                <CircularProgress />
            ) : !post ? (
                <Typography variant="h4" color="error" align="center">
                    Error!
                </Typography>
            ) : (
                <Typography variant="body1">{post.body}</Typography>
            )}
        </Container>
    )
}

export default Post

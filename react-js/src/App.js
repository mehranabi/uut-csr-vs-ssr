import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from './Home'
import Blog from './Blog'
import Post from './Post'

const App = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/post/:id">
                    <Post />
                </Route>
                <Route path="/blog">
                    <Blog />
                </Route>
                <Route path="/">
                    <Home />
                </Route>
            </Switch>
        </BrowserRouter>
    )
}

export default App

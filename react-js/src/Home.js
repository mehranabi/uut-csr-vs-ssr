import React from 'react'
import { Link } from 'react-router-dom'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

const Home = () => {
    return (
        <Container>
            <Typography variant="h5">Welcome!</Typography>
            <Link to="/blog">
                <Button variant="outlined">Blog</Button>
            </Link>
        </Container>
    )
}

export default Home

import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CircularProgress from '@material-ui/core/CircularProgress'

const Blog = () => {
    const [loading, setLoading] = useState(true)
    const [posts, setPosts] = useState([])

    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = async () => {
        setLoading(true)

        try {
            const response = await axios.get(
                'https://jsonplaceholder.typicode.com/posts',
                {
                    headers: {
                        Accept: 'application/json',
                    },
                }
            )
            setPosts(response.data)
        } catch (e) {}

        setLoading(false)
    }

    return (
        <Container>
            {loading ? (
                <CircularProgress />
            ) : (
                posts.map(({ id, title }) => {
                    return (
                        <Card className="post" key={id}>
                            <Link to={`/post/${id}`}>
                                <CardActionArea>
                                    <CardContent>
                                        <Typography variant="h6">
                                            {title}
                                        </Typography>
                                    </CardContent>
                                </CardActionArea>
                            </Link>
                        </Card>
                    )
                })
            )}
        </Container>
    )
}

export default Blog
